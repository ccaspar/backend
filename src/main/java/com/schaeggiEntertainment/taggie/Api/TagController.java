package com.schaeggiEntertainment.taggie.Api;

import com.schaeggiEntertainment.taggie.Entities.Student;
import com.schaeggiEntertainment.taggie.Entities.User;
import com.schaeggiEntertainment.taggie.Repositories.UserRepository;
import com.schaeggiEntertainment.taggie.model.enums.TagTypes;
import com.schaeggiEntertainment.taggie.service.StudentService;
import com.schaeggiEntertainment.taggie.service.UserService;
import com.schaeggiEntertainment.taggie.util.StudentUpdateDTO;
import com.schaeggiEntertainment.taggie.util.TagTypesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.lang.Math.max;


@RequestMapping("/student")
@RestController
@CrossOrigin(origins = {"*","**"}, allowedHeaders = {"*","**"},
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE})
public class TagController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private JdbcUserDetailsManager userDetailsManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService service;

    @PostMapping("/createUser")
    public UserDetails createUser(@RequestBody @NotNull User newUser){
        if (userRepository.findByUsername(newUser.getUsername()).isPresent()){
            throw new IllegalArgumentException("username already exists");
        }
        UserDetails user = org.springframework.security.core.userdetails.User.builder()
            .username(newUser.getUsername())
            .password(passwordEncoder.encode(newUser.getPassword()))
            .roles("USER")
            .build();
        userDetailsManager.createUser(user);
        return user;
    }
    @GetMapping("/login")
    private Student login(Principal principal) {
        return studentService.findByPrincipal(principal);
    }

    @GetMapping("/get")
    private Student login(@RequestParam(name = "u") String username) {
        return studentService.findByUsername(username);
    }
    @GetMapping("/getId")
    private Student loginId(@RequestParam(name = "u") Integer username) {
        return studentService.findById(username);
    }
    @GetMapping("/default")
    public Student defaultStudent() {
        return studentService.createDefault("cedric");
    }

    @PostMapping("/update")
    public Student finishUserproperties(@RequestBody StudentUpdateDTO update){
        System.out.println(update);
        return studentService.updateStudent(update);

    }
    @GetMapping("/graph")
    public void getGraph(){

    }
    @GetMapping("/create10")
    public void create10(){
        studentService.createDefault10();
    }


    @GetMapping("/all")
    public List<Student> allStudents() {
        return studentService.getAllStudents();
    }


}
