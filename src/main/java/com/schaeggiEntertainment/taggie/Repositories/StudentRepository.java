package com.schaeggiEntertainment.taggie.Repositories;

import com.schaeggiEntertainment.taggie.Entities.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, String> {
    Optional<Student> findByUsername(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM test.students s WHERE s.id_number = ?1")
    List<Student> findByNumberId(Long id);
}


