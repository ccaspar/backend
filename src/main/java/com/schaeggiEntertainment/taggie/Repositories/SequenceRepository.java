package com.schaeggiEntertainment.taggie.Repositories;

import com.schaeggiEntertainment.taggie.Entities.GeneralSequenceNumber;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SequenceRepository extends JpaRepository<GeneralSequenceNumber, Long> {
}
