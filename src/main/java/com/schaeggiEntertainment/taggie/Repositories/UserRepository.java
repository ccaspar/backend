package com.schaeggiEntertainment.taggie.Repositories;

import com.schaeggiEntertainment.taggie.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends JpaRepository<User, String> {
    List<User> findByUsernameIsNull();
    Optional<User> findByUsername(String name);
    List<User> findByUsernameLike(String name);
/*
    @Query(value = "select c.securityLevel from chatrooms_users c where c.userid=?1 and c.chatroomid=?2 and c.inviteAccepted=true",
            nativeQuery = true)
    Integer customGetSecurityLevel(Long u_id, Long c_id);

    @Query(value = "update chatrooms_users c set c.securityLevel=?3 where c.userid=?1 and c.chatroomid=?2 and c.inviteAccepted=true", nativeQuery = true)
    void customSetSecurityLevel(Long u_id, Long c_id, Integer lvl);

    @Query(value = "select u.u_id, u.email, u.username, u.password from (select c.userid from chatrooms_users c where c.chatroomid=?1 and c.securityLevel>=?2 and c.inviteAccepted=true) ids join users u on u.u_id=ids.userid", nativeQuery = true)
    List<User> customGetUserOfChatWithLvl(Long c_id, Integer lvl);

    @Query(value = "insert into chatrooms_users (chatroomid, userid, securityLevel, inviteAccepted) values (?2, ?1,?3, ?4);", nativeQuery = true)
    void customAddUserChatWithLvl(Long u_id, Long c_id, Integer lvl, Boolean inviteAccept);

    @Query(value = "select u.u_id, u.email, u.username, u.password from chatrooms chat join chatrooms_users con on chat.c_id=con.chatroomid join users u on u.u_id=con.userid where chat.c_id=?1 and con.securityLevel>=?2 and con.inviteAccepted=true and u.username like %?3%", nativeQuery = true)
    List<User> customGetUsersOfChatNameLike(Long chatId, Integer lvlNeeded, String nameLike);


    @Query(value = "update chatrooms_users c set c.inviteAccepted=?3 where c.chatroomid=?2 and c.userid=?1", nativeQuery = true)
    void customSetInviteByChat(Long userId, Long chatId, Boolean flag);

    @Query(value = "delete from chatrooms_users where chatroomid=?1", nativeQuery = true)
    void customDeleteByChat(Long chatId);

    @Query(value = "delete from chatrooms_users where userid=?1", nativeQuery = true)
    void customDeleteByUser(Long chatId);

    @Query(value = "delete from chatrooms_users where userid=?1 and chatroomid=?2", nativeQuery = true)
    void customDeleteConnection(Long userId, Long chatId); */
}
