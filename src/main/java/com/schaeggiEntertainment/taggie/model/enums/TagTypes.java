package com.schaeggiEntertainment.taggie.model.enums;

import java.util.Arrays;

public enum TagTypes {
    AorBQuestion, SimpleTextfield, Messaging;



    public static boolean isTagType(String s){
        return Arrays.toString(TagTypes.values()).contains(s);
    }

    public static String checkvalidity(String s){
        if (isTagType(s)) {
            return s;
        } else {
            throw new IllegalArgumentException("Not a valid tag type");
        }
    }

    public static String className(String s){
        if (isTagType(s)) {
            return "com.schaeggiEntertainment.taggie.Entities." + s;
        } else {
            throw new IllegalArgumentException(s + " is not a valid tag type");
        }
    }

    public static String[] getDescriptions(){
        return new String[] {
                "A or B Question", "One textfield", "chatroom"};
    }

}
