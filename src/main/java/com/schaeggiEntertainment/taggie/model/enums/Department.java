package com.schaeggiEntertainment.taggie.model.enums;

public enum Department {
    D_ARCH("D-ARCH"),
    D_BAUG("D_BAUG"),
    D_BSSE("D_BSSE"),
    D_INFK("D_INFK"),
    D_ITET("D_ITET"),
    D_MAVT("D_MAVT"),
    D_MATL("D_MATL"),
    D_BIOL("D_BIOL"),
    D_CHAP("D_CHAP"),
    D_PHYS("D_PHYS"),
    D_ERDW("D_ERDW"),
    D_USYS("D_USYS"),
    D_HEST("D_HEST"),
    D_MTEC("D_MTEC"),
    D_GESS("D_GESS");

    private final String name;

    Department(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameWithPrefix() {
        return "ROLE_" + name;
    }

}
