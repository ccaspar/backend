package com.schaeggiEntertainment.taggie.model.enums;

public enum Hobby {
    SPORT_FOOTBALL("SPORT_FOOTBALL"),
    SPORT_BASKETBALL("SPORT_BASKETBALL"),
    SPORT_HOCKEY("SPORT_HOCKEY"),
    SPORT_HICKING("SPORT_HICKING"),
    SPORT_JOGGING("SPORT_JOGGING"),
    SPORT_TENNIS("SPORT_TENNIS"),
    SPORT_WINTER("SPORT_WINTER"),
    SPORT_WATER("SPORT_WATER"),
    SPORT_GENERAL("SPORT_GENERAL"),
    GAMING("GAMING"),
    READING("READING"),
    DRINKING("DRINKING"),
    EATING("EATING"),
    D_MAVT("D_MAVT"),
    D_MATL("D_MATL"),
    D_BIOL("D_BIOL"),
    D_CHAP("D_CHAP"),
    D_PHYS("D_PHYS"),
    D_ERDW("D_ERDW"),
    D_USYS("D_USYS"),
    D_HEST("D_HEST"),
    D_MTEC("D_MTEC"),
    D_GESS("D_GESS");

    private final String name;

    Hobby(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameWithPrefix() {
        return "ROLE_" + name;
    }

}
