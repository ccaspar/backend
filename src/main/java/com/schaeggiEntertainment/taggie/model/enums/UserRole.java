package com.schaeggiEntertainment.taggie.model.enums;

public enum UserRole {

    PUBLIC("PUBLIC"),
    ADMIN("ADMIN");



    private String name;

    UserRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNameWithPrefix() {
        return "ROLE_" + name;
    }
}
