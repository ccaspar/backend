package com.schaeggiEntertainment.taggie.Config;

import com.schaeggiEntertainment.taggie.model.enums.UserRole;
import com.schaeggiEntertainment.taggie.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    private final CustomUserDetailsService customUserDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    DataSource dataSource;

    @Value("${server.publicPassword:taggie4ever}") String publicPw;
    @Value("${server.publicUser:taggiePUB}") String publicUser;
    @Value("${server.adminPassword:admin}") String adminPw;
    @Value("${server.adminUser:admin}") String adminUser;

    @Autowired
    public SecurityConfig(CustomUserDetailsService customUserDetailsService, PasswordEncoder passwordEncoder) {
        this.customUserDetailsService = customUserDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        //auth.inMemoryAuthentication()
        //        .withUser(this.adminUser).password(passwordEncoder.encode(this.adminPw)).roles(UserRole.PUBLIC.getName(), UserRole.ADMIN.getName());
        //        .and()
        //        .withUser(this.publicUser).password(passwordEncoder.encode(this.publicPw)).roles(UserRole.PUBLIC.getName());
        //auth.authenticationProvider(authenticationProvider());
        auth.jdbcAuthentication().dataSource(dataSource);
    }
    @Bean
    public JdbcUserDetailsManager getUsers() {
        return new JdbcUserDetailsManager(dataSource);

    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(customUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder);
        return authProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .cors().and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/student/**").permitAll()
                .anyRequest().fullyAuthenticated()
                .and().httpBasic()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }


    @Override
    public void configure(WebSecurity http) {
//        http.ignoring().antMatchers(HttpMethod.POST, "/readTemplate");
        http.ignoring().antMatchers(HttpMethod.GET, "/error/");
        http.ignoring().antMatchers(HttpMethod.OPTIONS, "**");
    }

}

