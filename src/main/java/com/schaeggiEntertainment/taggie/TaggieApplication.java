package com.schaeggiEntertainment.taggie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaggieApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaggieApplication.class, args);
	}

}
