package com.schaeggiEntertainment.taggie.Entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.schaeggiEntertainment.taggie.model.enums.Department;
import com.schaeggiEntertainment.taggie.model.enums.Hobby;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Getter
@Setter
@Table(name = "students")
//@JsonIgnoreProperties({"genTimestamp", "qrGenerated"})
public class Student {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @NotNull
    private String username;

    @OneToOne()
    private GeneralSequenceNumber id;

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @Email
    private String email;

    private LocalDate birth;

    private String description;
    private String gamerTag;
    private int imageId;
    private Department department;
    
    // ------ How am I -----
    @Max(1)
    @Min(-1)
    private short spFoot = 0;
    @Max(1)
    @Min(-1)
    private short spBasket = 0;
    @Max(1)
    @Min(-1)
    private short spHockey = 0;
    @Max(1)
    @Min(-1)
    private short spHike = 0;
    @Max(1)
    @Min(-1)
    private short spJog = 0;
    @Max(1)
    @Min(-1)
    private short spGym = 0;
    @Max(1)
    @Min(-1)
    private short spTennis = 0;
    @Max(1)
    @Min(-1)
    private short spWinter = 0;
    @Max(1)
    @Min(-1)
    private short spWater = 0;
    @Max(1)
    @Min(-1)
    private short spGeneral = 0;
    @Max(1)
    @Min(-1)
    private short gamingCasual = 0;
    @Max(1)
    @Min(-1)
    private short gamingLol = 0;
    @Max(1)
    @Min(-1)
    private short gamingTryhard = 0;
    @Max(1)
    @Min(-1)
    private short reading = 0;
    @Max(1)
    @Min(-1)
    private short drinking = 0;
    @Max(1)
    @Min(-1)
    private short eating = 0;
    @Max(1)
    @Min(-1)
    private short techno = 0;
    @Max(1)
    @Min(-1)
    private short computers = 0;
    @Max(1)
    @Min(-1)
    private short programming = 0;
    @Max(1)
    @Min(-1)
    private short anime = 0;
    @Max(1)
    @Min(-1)
    private short manga = 0;
    private boolean iosOrAndroid = false;
    @Max(1)
    @Min(-1)
    private short plants = 0;
    @Max(1)
    @Min(-1)
    private short cooking = 0;
    @Max(1)
    @Min(-1)
    private short maths = 0;
    @Max(1)
    @Min(-1)
    private short netflix = 0;
    @Max(1)
    @Min(-1)
    private short youtube = 0;
    @Max(1)
    @Min(-1)
    private short anDog = 0;
    @Max(1)
    @Min(-1)
    private short anCat = 0;
    @Max(1)
    @Min(-1)
    private short anOther = 0;

    @Max(1)
    @Min(-1)
    private short depressed = 0;
    @Max(1)
    @Min(-1)
    private short upEarly = 0;
    @Max(1)
    @Min(-1)
    private short party = 0;
    @Max(1)
    @Min(-1)
    private short chill = 0;

    @Max(1)
    @Min(-1)
    private short muClassic = 0;
    @Max(1)
    @Min(-1)
    private short muRap = 0;
    @Max(1)
    @Min(-1)
    private short muPop = 0;
    @Max(1)
    @Min(-1)
    private short muRock = 0;
    @Max(1)
    @Min(-1)
    private short muMetal = 0;

    @Max(1)
    @Min(-1)
    private short genderM = 0;
    @Max(1)
    @Min(-1)
    private short genderF = 0;
    @Max(1)
    @Min(-1)
    private short genderMix = 0;

    // ------ What do I want ---------

    @Max(1)
    @Min(-1)
    private short wGenderM = 0;
    @Max(1)
    @Min(-1)
    private short wGenderF = 0;
    @Max(1)
    @Min(-1)
    private short wGenderMix = 0;
    @Max(1)
    @Min(-1)
    private short wSex = 0;
    @Max(1)
    @Min(-1)
    private short wHookup = 0;
    @Max(1)
    @Min(-1)
    private short wDrink = 0;
    @Max(1)
    @Min(-1)
    private short wParty = 0;
    @Max(1)
    @Min(-1)
    private short wFriendship = 0;
    @Max(1)
    @Min(-1)
    private short wRelationship = 0;
    @Max(1)
    @Min(-1)
    private short wChill = 0;
    @Max(1)
    @Min(-1)
    private short wSport = 0;
    @Max(1)
    @Min(-1)
    private short wInterdepart = 0;
}
