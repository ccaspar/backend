package com.schaeggiEntertainment.taggie.Entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.schaeggiEntertainment.taggie.model.enums.UserRole;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Getter
@Setter
@Table(name = "users")
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "u_id")
public class User {


    @Id
    @Column(name = "username")
    @NotBlank
    private String username;

    @Column(name = "password")
    private String password;

    private boolean enabled;

    @Transient
    private String roles;

}
