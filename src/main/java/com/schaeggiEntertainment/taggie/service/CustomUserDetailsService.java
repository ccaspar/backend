package com.schaeggiEntertainment.taggie.service;

import com.schaeggiEntertainment.taggie.Entities.User;
import com.schaeggiEntertainment.taggie.Repositories.UserRepository;
import com.schaeggiEntertainment.taggie.model.enums.UserRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(s);
        if(userOptional.isPresent()){
            return new UserPrincipal(userOptional.get());
        }
        throw new UsernameNotFoundException(String.format("The User with name (/username) %s is not present in the DB!", s));
    }
}

