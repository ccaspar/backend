package com.schaeggiEntertainment.taggie.service;

import com.schaeggiEntertainment.taggie.Entities.GeneralSequenceNumber;
import com.schaeggiEntertainment.taggie.Entities.Student;
import com.schaeggiEntertainment.taggie.Repositories.*;
import com.schaeggiEntertainment.taggie.util.StudentUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.time.LocalDate;
import java.util.*;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SequenceRepository sequenceRepository;

    @Value("${server.graph.url}") String graphURL;

    @Autowired
    private MappingJackson2HttpMessageConverter springMvcJacksonConverter;

    private boolean detailedStates = true;
    public boolean hideStatisticEvents = false;

    public Student findByUsername(String name) {
        return studentRepository.findByUsername(name).orElseThrow(() -> new IllegalArgumentException("user is not initialized"));
    }
    public Student findById(Integer id){
        Optional<Student> os = studentRepository.findByNumberId((long) id).stream().findFirst();
        if (os.isEmpty())
            throw new IllegalArgumentException("User not found by Id");
        return os.get();

    }
    public Student findByPrincipal(Principal name) {
        return studentRepository.findByUsername(name.getName()).orElseThrow(() -> new IllegalArgumentException("user is not initialized"));
    }


    public boolean updateGraph() {
        RestTemplate r = new RestTemplate();
        return false;
    }
    public String getGraph(){
        RestTemplate r = new RestTemplate();
        ResponseEntity<String> response = r
                .exchange(graphURL, HttpMethod.GET, null, String.class);

        String body = response.getBody();
        System.out.println(body);
        return body;

    }
    public Student createDefault() {
        return createDefault("cedric");

    }
    public Student createDefault(String username) {
        if (studentRepository.findByUsername(username).isPresent()) {
            throw new IllegalArgumentException("username already exists");
        }
        GeneralSequenceNumber g = new GeneralSequenceNumber();
        sequenceRepository.save(g);
        Student s = Student.builder().username(username).firstName("Cedric").lastName("Caspar").email("ccaspar@student.ethz.ch").birth(LocalDate.of(1997,11,11))
                .computers((short) 1).id(g).build();
        studentRepository.saveAndFlush(s);
        return s;
    }
    public void createDefault10() {
        Random r = new Random();
        List<GeneralSequenceNumber> gs = new ArrayList<>();
        for (int i = 0;i <10; i++) {
            gs.add(new GeneralSequenceNumber());
        }
        sequenceRepository.saveAll(gs);

        List<Student> ss = new ArrayList<>();
        List<String> names = new ArrayList<>() {{add("alennon"); add("dawsonp"); add("ranworth"); add("sbritton"); add("esparrow"); add("dmadeleine"); add("pawelb"); add("mermontes"); add("eterrell"); add("zazhang");}};
        List<String> firsts = new ArrayList<>() {{add("Alyce"); add("Dawson"); add("Randall"); add("Sianna"); add("Estelle"); add("Madeleine"); add("Pawel"); add("Meredith"); add("Esmay"); add("Zayna");}};
        List<String> seconds = new ArrayList<>() {{add("Lennon"); add("Palacios"); add("Worthington"); add("Britton"); add("Sparrow"); add("Dean"); add("Barr"); add("Montes"); add("Terrell"); add("Zhang");}};
        int[] genderM = new int[] {0, 1, 1, 1, 0, 0, 1, 0, 1, 0};
        int[] genderF = new int[] {1, 0, 0 ,1, 1, 1, 0, 1, 0, 1};
        int[] genderB = new int[] {0, 0, 0, 1, 0, 0, 0, 1, 0, 0};
        for (int i = 0; i <10; i++) {
            ss.add(Student.builder().username(names.get(i)).firstName(firsts.get(i)).lastName(seconds.get(i)).email(names.get(i).concat("@student.ethz.ch")).birth(LocalDate.ofEpochDay(r.nextLong((long) 15000)))
                            .anCat((short)r.nextInt(-1, 1))
                    .depressed((short)r.nextInt(-1, 1))
                    .chill((short)r.nextInt(-1, 1))
                    .wChill((short)r.nextInt(-1, 1))
                    .drinking((short)r.nextInt(0, 1))
                    .party((short)r.nextInt(0, 1))
                    .gamingCasual((short)r.nextInt(-1, 0))
                            .manga((short)r.nextInt(-1, 1))
                    .computers((short)r.nextInt(-1, 1))
                    .programming((short)r.nextInt(-1, 1))
                    .cooking((short)r.nextInt(-1, 1))
                    .genderM((short)genderM[i])
                    .genderF((short)genderF[i])
                    .genderMix((short)genderB[i])
                            .spBasket((short)r.nextInt(-1, 1))
                            .spWinter((short)r.nextInt(-1, 1))
                    .spFoot((short)r.nextInt(-1, 1))
                    .spHike((short)r.nextInt(-1, 1))
                    .spGeneral((short)r.nextInt(-1, 1))
                    .muRock((short)r.nextInt(-1, 1))
                    .muMetal((short)r.nextInt(-1, 1))
                            .muClassic((short)r.nextInt(-1, 1))
                            .muRap((short)r.nextInt(-1, 1))
                    .id(gs.get(i)).build());
        }
        studentRepository.saveAllAndFlush(ss);

    }
    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    public Student updateStudent(StudentUpdateDTO update) {
        Optional<Student> s = studentRepository.findByUsername(update.getUsername());
        if (s.isPresent()) {
            Student ss = s.get();
            if (!ss.getUsername().equals(update.getUsername()) ||
                    !ss.getFirstName().equals(update.getFirstName()) ||
                    !ss.getLastName().equals(update.getLastName())) {
                System.out.println("Illegal");
                throw new IllegalArgumentException("Updates don't match static values");
            } else {
                if (update.getDescription() != null) {
                    ss.setDescription(update.getDescription());
                }
                if (update.getImageId() != null) {
                    ss.setImageId(update.getImageId());
                }
                ss = fillProperties(ss, update);
                System.out.println(ss);
                studentRepository.saveAndFlush(ss);
                return ss;
            }
        } else {
            GeneralSequenceNumber gs = new GeneralSequenceNumber();
            sequenceRepository.save(gs);

            Student ss = Student.builder().username(update.getUsername())
                    .firstName(update.getFirstName())
                    .lastName(update.getLastName())
                    .imageId(update.getImageId())
                    .description(update.getDescription()).id(gs).build();

            ss = fillProperties(ss, update);
            studentRepository.saveAndFlush(ss);
            return ss;
        }
    }
    private Student fillProperties(Student s, StudentUpdateDTO u){
        String[] all = new String[]{"spFoot", "spBasket", "spHockey", "spHike", "spJog", "spGym", "spTennis", "spWinter", "spWater", "gamingCasual", "gamingLol", "gamingTryhard", "reading", "drinking", "eating", "techno", "computers", "programming", "anime", "manga", "iosOrAndriod", "plants", "cooking", "maths", "netflix", "youtube", "anDog", "anCat", "anOther", "depressed", "upEarly", "party", "chill", "muClassic", "muRap", "muPop", "muRock", "muMetal", "genderM", "genderF", "genderMix"};
        String[] n = u.getNegatives();
        for (String value : n) {
            switch (value) {
                case "spFoot" -> s.setSpFoot((short) -1);
                case "spBasket" -> s.setSpBasket((short) -1);
                case "spHockey" -> s.setSpHockey((short) -1);
                case "spHike" -> s.setSpHike((short) -1);
                case "spJog" -> s.setSpJog((short) -1);
                case "spGym" -> s.setSpGym((short) -1);
                case "spTennis" -> s.setSpTennis((short) -1);
                case "spWinter" -> s.setSpWinter((short) -1);
                case "spWater" -> s.setSpWater((short) -1);
                case "gamingCasual" -> s.setGamingCasual((short) -1);
                case "gamingLol" -> s.setGamingLol((short) -1);
                case "gamingTryhard" -> s.setGamingTryhard((short) -1);
                case "reading" -> s.setReading((short) -1);
                case "drinking" -> s.setDrinking((short) -1);
                case "eating" -> s.setEating((short) -1);
                case "techno" -> s.setTechno((short) -1);
                case "computers" -> s.setComputers((short) -1);
                case "programming" -> s.setProgramming((short) -1);
                case "anime" -> s.setAnime((short) -1);
                case "manga" -> s.setManga((short) -1);
                case "iosOrAndriod" -> s.setIosOrAndroid(true);
                case "plants" -> s.setPlants((short) -1);
                case "cooking" -> s.setCooking((short) -1);
                case "maths" -> s.setMaths((short) -1);
                case "netflix" -> s.setNetflix((short) -1);
                case "youtube" -> s.setYoutube((short) -1);
                case "anDog" -> s.setAnDog((short) -1);
                case "anCat" -> s.setAnCat((short) -1);
                case "anOther" -> s.setAnOther((short) -1);
                case "depressed" -> s.setDepressed((short) -1);
                case "upEarly" -> s.setUpEarly((short) -1);
                case "party" -> s.setParty((short) -1);
                case "chill" -> s.setChill((short) -1);
                case "muClassic" -> s.setMuClassic((short) -1);
                case "muRap" -> s.setMuRap((short) -1);
                case "muPop" -> s.setMuPop((short) -1);
                case "muRock" -> s.setMuRock((short) -1);
                case "muMetal" -> s.setMuMetal((short) -1);
                case "genderM" -> s.setGenderM((short) -1);
                case "genderF" -> s.setGenderF((short) -1);
                case "genderMix" -> s.setGenderMix((short) -1);
                default -> throw new IllegalArgumentException("not matched" + value);
            }
        }
        String[] p = u.getPositives();
        for (String value : p) {
            switch (value) {
                case "spFoot" -> s.setSpFoot((short) 1);
                case "spBasket" -> s.setSpBasket((short) 1);
                case "spHockey" -> s.setSpHockey((short) 1);
                case "spHike" -> s.setSpHike((short) 1);
                case "spJog" -> s.setSpJog((short) 1);
                case "spGym" -> s.setSpGym((short) 1);
                case "spTennis" -> s.setSpTennis((short) 1);
                case "spWinter" -> s.setSpWinter((short) 1);
                case "spWater" -> s.setSpWater((short) 1);
                case "gamingCasual" -> s.setGamingCasual((short) 1);
                case "gamingLol" -> s.setGamingLol((short) 1);
                case "gamingTryhard" -> s.setGamingTryhard((short) 1);
                case "reading" -> s.setReading((short) 1);
                case "drinking" -> s.setDrinking((short) 1);
                case "eating" -> s.setEating((short) 1);
                case "techno" -> s.setTechno((short) 1);
                case "computers" -> s.setComputers((short) 1);
                case "programming" -> s.setProgramming((short) 1);
                case "anime" -> s.setAnime((short) 1);
                case "manga" -> s.setManga((short) 1);
                case "iosOrAndriod" -> s.setIosOrAndroid(true);
                case "plants" -> s.setPlants((short) 1);
                case "cooking" -> s.setCooking((short) 1);
                case "maths" -> s.setMaths((short) 1);
                case "netflix" -> s.setNetflix((short) 1);
                case "youtube" -> s.setYoutube((short) 1);
                case "anDog" -> s.setAnDog((short) 1);
                case "anCat" -> s.setAnCat((short) 1);
                case "anOther" -> s.setAnOther((short) 1);
                case "depressed" -> s.setDepressed((short) 1);
                case "upEarly" -> s.setUpEarly((short) 1);
                case "party" -> s.setParty((short) 1);
                case "chill" -> s.setChill((short) 1);
                case "muClassic" -> s.setMuClassic((short) 1);
                case "muRap" -> s.setMuRap((short) 1);
                case "muPop" -> s.setMuPop((short) 1);
                case "muRock" -> s.setMuRock((short) 1);
                case "muMetal" -> s.setMuMetal((short) 1);
                case "genderM" -> s.setGenderM((short) 1);
                case "genderF" -> s.setGenderF((short) 1);
                case "genderMix" -> s.setGenderMix((short) 1);
                default -> throw new IllegalArgumentException("not matched" + value);
            }
        }
        Collection<String> rest = new HashSet<>(Arrays.asList(n));
        rest.addAll(Arrays.asList(p));
        Collection<String> alls = new HashSet<>(Arrays.asList(all));
        alls.stream().filter(rest::contains).forEach((String value)->{
            switch (value) {
                case "spFoot" -> s.setSpFoot((short) 0);
                case "spBasket" -> s.setSpBasket((short) 0);
                case "spHockey" -> s.setSpHockey((short) 0);
                case "spHike" -> s.setSpHike((short) 0);
                case "spJog" -> s.setSpJog((short) 0);
                case "spGym" -> s.setSpGym((short) 0);
                case "spTennis" -> s.setSpTennis((short) 0);
                case "spWinter" -> s.setSpWinter((short) 0);
                case "spWater" -> s.setSpWater((short) 0);
                case "gamingCasual" -> s.setGamingCasual((short) 0);
                case "gamingLol" -> s.setGamingLol((short) 0);
                case "gamingTryhard" -> s.setGamingTryhard((short) 0);
                case "reading" -> s.setReading((short) 0);
                case "drinking" -> s.setDrinking((short) 0);
                case "eating" -> s.setEating((short) 0);
                case "techno" -> s.setTechno((short) 0);
                case "computers" -> s.setComputers((short) 0);
                case "programming" -> s.setProgramming((short) 0);
                case "anime" -> s.setAnime((short) 0);
                case "manga" -> s.setManga((short) 0);
                case "iosOrAndriod" -> s.setIosOrAndroid(true);
                case "plants" -> s.setPlants((short) 0);
                case "cooking" -> s.setCooking((short) 0);
                case "maths" -> s.setMaths((short) 0);
                case "netflix" -> s.setNetflix((short) 0);
                case "youtube" -> s.setYoutube((short) 0);
                case "anDog" -> s.setAnDog((short) 0);
                case "anCat" -> s.setAnCat((short) 0);
                case "anOther" -> s.setAnOther((short) 0);
                case "depressed" -> s.setDepressed((short) 0);
                case "upEarly" -> s.setUpEarly((short) 0);
                case "party" -> s.setParty((short) 0);
                case "chill" -> s.setChill((short) 0);
                case "muClassic" -> s.setMuClassic((short) 0);
                case "muRap" -> s.setMuRap((short) 0);
                case "muPop" -> s.setMuPop((short) 0);
                case "muRock" -> s.setMuRock((short) 0);
                case "muMetal" -> s.setMuMetal((short) 0);
                case "genderM" -> s.setGenderM((short) 0);
                case "genderF" -> s.setGenderF((short) 0);
                case "genderMix" -> s.setGenderMix((short) 0);
                default -> throw new IllegalArgumentException("not matched" + value);

            };});

        return s;
    }

}

