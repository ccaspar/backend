package com.schaeggiEntertainment.taggie.error;

public class TagInUseException extends IllegalArgumentException {

    public TagInUseException() {
        super();
    }

    public TagInUseException(String s) {
        super(s);
    }
    public TagInUseException(String message, Throwable cause) {
        super(message, cause);
    }

}
