package com.schaeggiEntertainment.taggie.error;

public class TagUpdateNotAllowed extends IllegalArgumentException {

    public TagUpdateNotAllowed() {
        super();
    }

    public TagUpdateNotAllowed(String s) {
        super(s);
    }
    public TagUpdateNotAllowed(String message, Throwable cause) {
        super(message, cause);
    }

}