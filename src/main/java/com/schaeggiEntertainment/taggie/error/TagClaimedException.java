package com.schaeggiEntertainment.taggie.error;

public class TagClaimedException extends IllegalArgumentException {

    public TagClaimedException() {
        super();
    }

    public TagClaimedException(String s) {
        super(s);
    }
    public TagClaimedException(String message, Throwable cause) {
        super(message, cause);
    }

}
