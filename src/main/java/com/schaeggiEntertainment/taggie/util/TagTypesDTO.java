package com.schaeggiEntertainment.taggie.util;


import com.schaeggiEntertainment.taggie.model.enums.TagTypes;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TagTypesDTO {

    public List<TagTypes> types;

    public String[] descriptions;
}
