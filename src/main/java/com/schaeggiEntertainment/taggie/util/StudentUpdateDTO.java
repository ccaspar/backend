package com.schaeggiEntertainment.taggie.util;

import com.schaeggiEntertainment.taggie.model.enums.Department;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class StudentUpdateDTO {

    @NotNull
    private String username;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String description;
    private Integer imageId;
    private LocalDate birth;
    private String email;
    private String gamerTag;
    private Department department;
    private String[] positives;
    private String[] negatives;
}
