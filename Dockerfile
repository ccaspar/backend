FROM maven:3.8.5-eclipse-temurin-17-alpine AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -DskipTests -Pprod

#
# Package stage
#
FROM openjdk:17-alpine
COPY --from=build /home/app/target/taggie-0.0.1-SNAPSHOT.jar /usr/local/lib/backend.jar
EXPOSE 8080:8080
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=prod","/usr/local/lib/backend.jar"]

